function onCloseMenu($target) {
  $target.removeClass('is-open');
  $('body').removeClass('is-open-menu');
}

function onOpenMenu($target) {
  $target.addClass('is-open');
  $('body').addClass('is-open-menu');
}

function prevStep({path, pos, steps, $target}) {
  let _pos = pos;
  _pos--;
  if (_pos < 0) return;
  path.animate({
    'd': steps[_pos]
  }, 60, mina.linear, function() {
    // if (_pos === 0) {
    //   onCloseMenu($target);
    // }
    prevStep({path, pos:_pos, steps, $target});
  });
  onCloseMenu($target);
};

function nextStep({path, pos, steps, stepsNb}) {
  let _pos = pos;
  _pos++;
  if (_pos > stepsNb) {

    return;
  }
  path.animate({
    d: steps[_pos]
  }, 60, mina.easeinout, function() {
    nextStep({path, pos:_pos, steps, stepsNb});
  });
};

export {onCloseMenu, onOpenMenu, prevStep, nextStep};